# An Introduction to UAV System

**Author**：*丁瑞洋*

## 系统架构

![1](1.png)

该系统的目的是对DJI sdk的API进行更好的封装，同时加入对其他传感器（zed双目相机等）的接口进行调用的API，以及飞行状态显示，飞行数据记录等功能，为之后的视觉导航算法提供平台

系统的组成以及大体功能由上图所示，注意由于在两个板子之间存在socket连接，因此可以在tx1直接调用UPBOARD里的api：整个调用的过程如下：
1. 用户调用AuFlightAPI中的飞行控制api接口
2. api函数将AuMessageHeader类的命令通过socket（经AuSocketClient类）传到upboard
3. upbaord的AuServerThread接收到命令，通过调用对应的Run函数（例如：RunTakeoff）来调用对应的AuDJIFlightControl或者AuDJISensor类中的函数，从而实现对飞机的控制。

### UPBOARD

Upboard负责底层的控制，对应项目 flightserver/flightserverapi

 #### 包含的类及其功能 

* AuMatrix

  定义了一个矩阵以及相关操作

* AuBgTaskThread 

  定义了一个背景线程

* AuMessage

  定义了一组用于表示控制信号的结构体，这些结构体通过socket接口在upboard和tx1之间传输

* AuCamera

  定义了相机的行为

* AuMessageQueue

  定义了一个消息队列， 带互斥锁，由于无人机的控制不是异步的，需要等上一个命令执行完毕之后再执行下一个，所以必须要有一个队列

* AuContainer1D/2D/3D    

  定义了一个一维/二维/三维容器

* AuDJIFlightAPI

  定义了获取当前飞行姿态（通过AuDJISensor），下达飞行命令（通过AuDJIFlightControl）， 路径点规划（通过AuDJIPathFollowThread）的接口，

* AuServerThread

  定义了整个flightserver的行为，为主线程

* AuDJIFlightControl

  与DJI SDK底层交互的接口，用于下达飞行命令

* AuSocket

  定义了Socket接口，该类存在的意义在于在直接调用系统socket之前进行参数合法性以及安全性检查。用于两个板子之间数据的传输

* AuDJIPathFollowThread

  是AuThread的子类，当调用AuDJIFlightAPI中的BeginPathFollow的时候，创建并启动该线程，使得无人机按照指定路径飞行

* AuString

  定义了一个String，比stl里的string加了一些新的功能

* AuDJISensor

  与DJI SDK底层交互的接口，用于获取传感器数据

* AuThread

  定义一个线程的基本行为，定义后台线程的类均由它派生

* AuTime

  重写了<time.h>里的时间功能，定义了获取当前时刻，以及计时等功能

* AuDefine

  各种宏定义

* AuVector

  重写了Engin里的向量，定义了一系列向量运算操作

* AuFile

  文件的读写

* AuFlight

  定义了无人机的一系列接口，通过对AuFlightAPI内的函数进行调用

* AuFlightLog

  用于写飞行log

* AuFlightUtility

  定义了一系列飞行路径，如飞方形等

* AuImage

  定义了对获取的图像的一系列操作

* AuList

  定义了一个单向链表

### TX1

Upboard负责顶层的控制，对应项目 flightapi

#### 包含的类及其功能

  *大多与upboard重复，以下主要介绍与upboard不同的类*

* AuFlightAPI

    成员函数名同upboard，但是不同的是，TX1版的该类不与dji sdk进行交互，而是从消息队列里取/写入命令或者消息

*  AuScreen 

    定义了一个图形界面（基于opengl），可以显示飞行轨迹和飞行状态
* AuTexture/AuGL

    包装了OpenGL常用函数，定义了OpenGL中的材质

* AuZED
  定义了ZED双目摄像头，以及获取深度图/点云/rgb图像等常用操作




